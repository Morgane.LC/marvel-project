import { createStore } from "vuex";
import axios from 'axios';
import md5 from 'md5';

export default createStore({
  state: {
    infosPerso: null,
    pageNumber: 1,
    loading: true,
  },
  getters: {
    getInfoPerso(state) {
      return state.infosPerso
    },
    getTotalPerso(state) {
      return state.totalPerso
    },
    getTotalPage(state) {
      return Math.floor(state.totalPerso / 20)
    },
    getPageNumber(state) {
      return state.pageNumber
    },
    getPrePageNumber(state) {
      return state.pageNumber - 1
    },
    getNextPageNumber(state) {
      return state.pageNumber + 1
    }
  },
  mutations: {
    setInfosPerso(state, infos) {
      state.infosPerso = infos;
    },
    setTotalPerso(state, number) {
      state.totalPerso = number
    },
    setPageNumber(state, page) {
      state.pageNumber = page
    },
    setPrePageNumber(state, page) {
      state.pageNumber = page > 1 ? page - 1 : 1
    },
    setNextPageNumber(state, page) {
      state.pageNumber = page < state.totalPerso ? page + 1 : state.totalPerso 
    }
  },
  actions: {
    async fetchApi({state, commit}) {
      const {
        VUE_APP_MARVEL_API_KEY_PUBLIC: API_PUBLIC_KEY,
        VUE_APP_MARVEL_API_KEY_PRIVATE: API_PRIVATE_KEY
      } = process.env;

      const timestamp = 1
      const hash = md5(`${timestamp}${API_PRIVATE_KEY}${API_PUBLIC_KEY}`)
      const offset = state.pageNumber === 1 ? 0 : state.pageNumber * 20;

      axios
        .get(`https://gateway.marvel.com/v1/public/characters?ts=${timestamp}&apikey=${API_PUBLIC_KEY}&hash=${hash}&offset=${offset}`)
        .then(response => {
          commit('setInfosPerso', response.data.data.results);
          commit('setTotalPerso', response.data.data.total)
        })
        .catch(error => {
          console.log(error)
        })
        .finally(() => state.loading = false)
    }
  },
})
